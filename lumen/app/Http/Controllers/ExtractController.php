<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Extract;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class ExtractController extends Controller
{
    private $user;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // GET CURRENT USER
        $this->user = auth()->user();
    }

    public function getExtract(Request $request) {
        $extracts = Extract::where('userId', $this->user->id)->orderBy('created_at', 'DESC')->get();

        foreach ($extracts as $key => $extract) {
            $extracts[$key]['id'] = $extract->_id;
            $extracts[$key]['createdAt'] = $extract->created_at;
        }

        return response()->json($extracts);
    }

    public function getVolume(Request $request) {

        //GET SELL'S
        $sellAmount = Extract::where([
            ['userId', $this->user->id],
            ['type', 'liquidation'],
            ['created_at', '>', Carbon::now()->startOfDay()]
        ])->sum('value');

        // GET BUY'S
        $buyAmount = Extract::where([
            ['userId', $this->user->id],
            ['type', 'investment'],
            ['created_at', '>', Carbon::now()->startOfDay()]
        ])->sum('value')*-1;

        return response()->json(['buy' => $buyAmount, 'sell' => $sellAmount]);
    }

}
