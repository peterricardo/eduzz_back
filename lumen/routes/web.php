<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

// ROUTES WITHOUT JWT
$router->group(['prefix' => '/'], function () use ($router) {
    $router->post('login', 'AuthController@login');
    $router->post('account', 'AuthController@register');
});

// ROUTES WITH JWT
$router->group(['prefix' => '/btc', 'middleware' => 'auth'], function () use ($router) {
    $router->get('/', 'OrderController@getPosition');
    $router->get('/price', 'OrderController@getPrice');
    $router->post('/purchase', 'OrderController@Purchase');
    $router->post('/sell', 'OrderController@Sell');
});

$router->group(['prefix' => 'account', 'middleware' => 'auth'], function () use ($router) {
    $router->post('deposit', 'UserController@deposit');
    $router->get('balance', 'UserController@getBalance');
});

$router->group(['prefix' => 'extract', 'middleware' => 'auth'], function () use ($router) {
    $router->get('/', 'ExtractController@getExtract');
});

$router->group(['prefix' => 'history', 'middleware' => 'auth'], function () use ($router) {
    $router->get('/', 'HistoryController@getHistory');
});

$router->group(['prefix' => 'volume', 'middleware' => 'auth'], function () use ($router) {
    $router->get('/', 'ExtractController@getVolume');
});
