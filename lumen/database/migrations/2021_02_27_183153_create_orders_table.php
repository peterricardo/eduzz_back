<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('userId');
            $table->decimal('purchasedAmount',12,2);
            $table->decimal('purchasedPrice',12,5);
            $table->decimal('purchasedBtcAmount',12,7);
            $table->timestamp('purchasedDate')->useCurrent();
            $table->timestamp('liquidatedAt')->nullable();
            $table->decimal('liquidatedPrice',12,5)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
