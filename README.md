# Eduzz Back Challenge
API created for Eduzz Back Challenge with:
- Lumen
- MongoDB
- Docker

## Required

- Docker

## Setup Instructions

You must rename the file `.env.example` to `.env`

### 1) Run the file `run.sh` from root folder:

`sh ./run.sh`

This command will execute these steps:

1) Build

2) Lumen Install

3) Run migrations to create tables

4) Start Lumen Cron

5) The environment can be accessed at: http://localhost:8081

### Postman Collection

The BackChallengeEduzz.postman_collection.json contains all endpoint's for this aplication

### Important

Check if you don't have any other process running at ports: 8081, 8181, 9000 and 27018 because these ports will be used by Docker.