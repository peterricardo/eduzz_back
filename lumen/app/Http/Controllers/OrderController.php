<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\{Order, Extract};
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class OrderController extends Controller
{
    private $user;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // GET CURRENT USER
        $this->user = auth()->user();
    }

    public function getPosition(Request $request) {
        $currentBtc = getBtcPrice();

        $orders = Order::where('userId', $this->user->id)->whereNull('liquidatedAt')->get();

        foreach ($orders as $key => $order) {
            $currentBtcAmount = $order->purchasedAmount/(float) $currentBtc['ticker']['buy'];
            $currentBtcPrice = (float) $currentBtc['ticker']['buy'];
            $orders[$key]['id'] = $order->_id;
            $orders[$key]['currentBtcPrice'] = $currentBtcPrice;
            $orders[$key]['currentBtcAmount'] = round($currentBtcAmount, 7);
            $orders[$key]['purchasedBtcAmount'] = $order->purchasedBtcAmount;
            $orders[$key]['purchasedAmount'] = $order->purchasedAmount;
            $orders[$key]['variation'] = round($orders[$key]['purchasedBtcAmount']/$orders[$key]['currentBtcAmount'], 5);
            $orders[$key]['sellAmount'] = round((float) $order->purchasedBtcAmount * $currentBtcPrice, 7);
            $orders[$key]['purchasedDate'] = $order->created_at;
        }

        return response()->json($orders);
    }

    public function getPrice(Request $request) {

        $currentBtc = getBtcPrice();

        return response()->json(['buy' => (float) $currentBtc['ticker']['buy'], 'sell' => (float) $currentBtc['ticker']['sell']]);
    }

    public function Purchase(Request $request) {
        $this->validate($request, [
            'amount' => 'required'
        ]);

        $amount = $request->input('amount');

        // CHECK IF USER HAS FOUNDS TO PURCHASE
        if($this->user->balance < $amount) {
            return response()->json([
                "statusCode" => 400,
                "message" => "you don't have the required amount at your account."
            ], 400);
        }

        $currentBtc = getBtcPrice();

        $purchasePrice = $currentBtc['ticker']['buy'];

        $order = new Order;

        $order->userId = $this->user->id;
        $order->purchasedAmount = $amount;
        $order->purchasedPrice = (float) $purchasePrice;
        $order->purchasedBtcAmount = round($amount/$purchasePrice, 7);
        $order->liquidatedAt = NULL;

        $order->save();

        // SAVE AT EXTRACT
        $extract = new Extract;
        $extract->userId = $this->user->id;
        $extract->type = 'investment';
        $extract->value = $request->amount*-1;
        $extract->save();

        // DEBIT VALUE FROM USER BALANCE
        $this->user->balance = $this->user->balance - $amount;
        $this->user->save();

        $content = loadEmailTemplate('purchase');
        $content = str_replace("{name}",$this->user->name, $content);
        $content = str_replace("{value}",formatBRCurrency($request->amount), $content);
        $content = str_replace("{bitcoin_value}",format_amount_with_no_e($order->purchasedBtcAmount) , $content);

        sendEmail($this->user->email, $this->user->name, 'Nova Compra', $content);
        return response()->json($order, 201);
    }

    public function reInvest($oldOrder, $balance) {

        $order = new Order;

        $amount = $balance;

        $order->userId = $this->user->id;
        $order->purchasedAmount = $amount;
        $order->purchasedPrice = $oldOrder->purchasedPrice;
        $order->purchasedBtcAmount = round($amount/$order->purchasedPrice, 7);
        $order->liquidatedAt = NULL;

        $order->save();

        // SAVE AT EXTRACT
        $extract = new Extract;
        $extract->userId = $this->user->id;
        $extract->type = 'investment';
        $extract->value =  $amount*-1;
        $extract->save();

    }

    public function Sell(Request $request) {
        $this->validate($request, [
            'amount' => 'required'
        ]);

        $amount = $request->input('amount');

        $currentBtc = getBtcPrice();

        // LOAD ORDERS
        $orders = Order::where('userId', $this->user->id)->whereNull('liquidatedAt')->get();

        // CHECK USER'S FUNDS
        $userFunds = 0;
        foreach ($orders as $order) {
            $currentBtcAmount = $order->purchasedAmount/(float) $currentBtc['ticker']['buy'];
            $currentBtcPrice = (float) $currentBtc['ticker']['buy'];

            $userFunds += (float) $order->purchasedBtcAmount * $currentBtcPrice;

        }

        // CHECK IF USER HAS SUFFICIENT FUNDS TO SELL
        if($amount > $userFunds)
            return response()->json([
                'statusCode' => 400,
                'message' => 'You dont have the required amount'
            ], 400);

        // LIQUID ORDERS
        $auxAmount = $amount;
        $soldAmount = 0;
        $ordersAmount = 0;

        foreach ($orders as $order) {
            $sellAmount = (float) $order->purchasedBtcAmount * $currentBtcPrice;
            $ordersAmount += $sellAmount;

            $order->liquidatedAt = Carbon::now();
            $order->liquidatedPrice = $currentBtcPrice;
            $order->save();

            if($sellAmount < $auxAmount) {
                $soldAmount += $sellAmount;
            } else { // REINVEST
                $soldAmount += $auxAmount;
                $this->reInvest($order, ($sellAmount - $auxAmount));
                break;
            }

            $auxAmount -= $sellAmount;
        }

        // SAVE AT EXTRACT
        $extract = new Extract;
        $extract->userId = $this->user->id;
        $extract->type = 'liquidation';
        $extract->value = $ordersAmount;
        $extract->save();

        // RETURN VALUE TO USER'S BALANCE
        $this->user->balance = $this->user->balance + $soldAmount;
        $this->user->save();

        $content = loadEmailTemplate('sell');
        $content = str_replace("{name}",$this->user->name, $content);
        $content = str_replace("{bitcoin_value}",format_amount_with_no_e($soldAmount/$currentBtcPrice) , $content);
        $content = str_replace("{value}",formatBRCurrency($soldAmount), $content);


        sendEmail($this->user->email, $this->user->name, 'Nova Venda', $content);

        return response()->json(['status' => 'success', 'type' => 'Sell', 'sellAmount' => Carbon::now()]);
    }
}
