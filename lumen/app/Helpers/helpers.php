<?php

use Illuminate\Support\Facades\Http;

function getBtcPrice() {
    $response = Http::get('https://www.mercadobitcoin.net/api/BTC/ticker/');
    return $response->json();
}

function loadEmailTemplate($type) {
    $content;
    switch ($type) {
        case 'welcome':
            $content = file_get_contents('welcomeEmail.html');
            break;
        case 'deposit':
            $content = file_get_contents('depositEmail.html');
            break;
        case 'purchase':
            $content = file_get_contents('purchaseEmail.html');
            break;
        case 'sell':
            $content = file_get_contents('sellEmail.html');
            break;
    }

    return $content;
}

function sendEmail($to, $name, $subject, $content) {
    return true;
    $email = new \SendGrid\Mail\Mail();

    $email->setFrom(getenv('SENDGRID_FROM_EMAIL'), "Bitcoin Mkt");
    $email->setSubject($subject);
    $email->addTo($to, $name);
    $email->addContent(
        "text/html", $content
    );
    $sendgrid = new \SendGrid(getenv('SENDGRID_API_KEY'));
    try {
        $response = $sendgrid->send($email);
    } catch (Exception $e) {
        echo 'Caught exception: '. $e->getMessage() ."\n";
    }
}

function formatBRCurrency($value) {
    return number_format($value,2,",",".");

}

function format_amount_with_no_e($amount) {
    $amount = (string)$amount; // cast the number in string
    $pos = stripos($amount, 'E-'); // get the E- position
    $there_is_e = $pos !== false; // E- is found

    if ($there_is_e) {
        $decimals = intval(substr($amount, $pos + 2, strlen($amount))); // extract the decimals
        $amount = number_format($amount, $decimals, '.', ','); // format the number without E-
    }

    return $amount;
}
