<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;


class History extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'buy',
        'sell'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'updated_at', "_id"
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'buy' => 'double',
        'sell' => 'double',
    ];

    protected $dates = ['created_at', 'updated_at'];
}
