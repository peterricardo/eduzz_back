<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;


class Order extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'userId',
        'purchasedAmount',
        'purchasedPrice',
        'purchasedBtcAmount',
        'purchasedDate',
        'liquidatedAt',
        'liquidatedPrice'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        '_id', 'created_at', 'updated_at', 'userId', 'liquidatedAt'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'purchasedAmount' => 'double',
        'purchasedPrice' => 'double',
        'liquidatedPrice' => 'double',
    ];

    protected $dates = ['created_at', 'updated_at', 'liquidatedAt'];
}
