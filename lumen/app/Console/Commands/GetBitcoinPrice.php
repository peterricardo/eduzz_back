<?php

namespace App\Console\Commands;

use App\Models\History;

use Illuminate\Console\Command;

class GetBitcoinPrice extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bitcoin:getprice';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get Bitcoin Price';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // GET CURRENT BITCOIN PRICE
        $currentBtc = getBtcPrice();

        $history = new History();
        $history->buy = (float) $currentBtc['ticker']['buy'];
        $history->sell = (float) $currentBtc['ticker']['sell'];
        $history->save();

        echo $history;
    }
}
