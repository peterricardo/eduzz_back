<?php

namespace App\Console\Commands;

use App\Models\History;

use Illuminate\Console\Command;

class PurgeOldHistory extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'history:purge';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Purge all history created more than 90 days ago.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        History::where('created_at', '<', Carbon::now()->subDays(90))->delete();

        echo "clean old history";
    }
}
