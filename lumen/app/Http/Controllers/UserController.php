<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\{User, Extract};
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use App\Mail\{Welcome};

class UserController extends Controller
{
    private $user;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // GET CURRENT USER
        $this->user = auth()->user();
    }

    public function deposit(Request $request) {
        $this->validate($request, [
            'amount' => 'required|numeric|min:1|not_in:0'
        ]);

        // UPDATE BALANCE
        $this->user->balance = $this->user->balance + $request->amount;
        $this->user->save();

        // SAVE AT EXTRACT
        $extract = new Extract;
        $extract->userId = $this->user->id;
        $extract->type = 'deposit';
        $extract->value = $request->amount;
        $extract->save();

        $content = loadEmailTemplate('deposit');
        $content = str_replace("{name}",$this->user->name, $content);
        $content = str_replace("{value}",formatBRCurrency($request->amount), $content);

        sendEmail($this->user->email, $this->user->name, 'Novo Depósito', $content);

        return response()->json(['balance' => $this->user->balance]);
    }

    public function getBalance(Request $request) {
        return response()->json(['balance' => $this->user->balance]);
    }
}
