<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\History;
use Carbon\Carbon;

class HistoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    public function getHistory(Request $request) {

        $query = History::where([
            ["created_at",">",Carbon::now()->subDay()],
            ["created_at","<",Carbon::now()]
            ])->orderBy('created_at', 'DESC')->get();

        $map = $query->map(function($items){
            $data['buy'] = $items->buy;
            $data['sell'] = $items->sell;
            $data['createdAt'] = $items->created_at;
            return $data;
        });

        return response()->json($map);
    }

}
