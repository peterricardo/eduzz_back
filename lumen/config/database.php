<?php
    return [
        'default' => 'mongodb',
        'connections' => [
            'mongodb' => array(
                'driver'   => 'mongodb',
                'host'     => env('DB_HOST', 'localhost'),
                'port'     => env('DB_PORT', 27017),
                'username' => env('DB_PASSWORD'),
                'password' => env('DB_USERNAME'),
                'database' => env('DB_DATABASE'),
                'options' => [
                    'database' => 'admin' // sets the authentication database required by mongo 3
                ]
            ),
        ],

        'migrations' => 'migrations',
    ];
